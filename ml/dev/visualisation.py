import numpy as np
import torch
import random

"""
Visualisation methods, mainly for testing and debugging

Main file is "ml_train.py"
"""


# debug helpers
def check_one(vid, comp_glob, comp_loc):
    ''' prints global and local scores for one video '''
    print("all we have on video: ", vid)
    for score in comp_glob:
        if score[0]==vid:
            print(score)
    for score in comp_loc:
        if score[1]==vid:
            print(score)

def seedall(s):
    ''' seeds all sources of randomness '''
    reproducible = (s >= 0)
    torch.manual_seed(s)
    random.seed(s)
    np.random.seed(s)
    torch.backends.cudnn.deterministic = reproducible
    torch.backends.cudnn.benchmark     = not reproducible
    print("\nSeeded all to", s)

def disp_one_by_line(it):
    ''' prints one iteration by line '''
    for obj in it:
        print(obj)

def disp_fake_pred(fakes, preds):
    print("FAKE PREDICTED")
    diff = 0
    for fake, pred in zip(fakes, preds):
        f, p = round(fake, 2), pred[2]
        print(f, p)
        diff += 100 * abs(f - p)**2
    print('mean dist**2 =', diff/len(preds))

def measure_diff(fakes, preds):
    """ Measures difference between ground truth and predicition
    
    fakes (float array): generated "true" global scores
    preds (list list): list of [video_id: int, criteria_name: str, 
                                    score: float, uncertainty: float]
                            in same order

    Returns:
        (float): 100 times mean squared distance 
                    between ground truth and predicted score
    """
    diff = 0
    for fake, pred in zip(fakes, preds):
        f, p = round(fake, 2), pred[2]
        diff += 100 * abs(f - p)**2
    return diff/len(preds)