from ml.dev.fake_data import generate_data
from ml.dev.visualisation import measure_diff, seedall
from ml.core import ml_run

"""
Testing results of ml algorithm by generating data

"""
def test_scores():
    """ trains for some epochs with generated data, 
    tests distribution after convergence
    """
    fake_data, glob_fake, loc_fake = generate_data(30, 200, 10, dens=0.8, 
                                                                scale=0.5, 
                                                                noise=0.001)
    glob_scores, contributor_scores = ml_run(   fake_data,
                                                40,
                                                ["reliability"], 
                                                resume=False,
                                                verb=0)
    diff = measure_diff(glob_fake, glob_scores) # mean squared distance * 100
    assert diff < 2, 'Predicted scores are far from real values'
    # assert 0 < min(s_list) < 10 #TODO get s_list (s parameter of each node)
