from os import makedirs
from numpy import disp
import torch

from ..data_utility import save_to_json, load_from_json
from .plots import plot_metrics, plot_density
from .visualisation import seedall, check_one, disp_one_by_line, disp_fake_pred
from .fake_data import generate_data

"""
Not used in production, for testing only
Module called from "ml_train.py" only if env var TOURNESOL_DEV is True

Used to perform some tests on ml algorithm (custom data, plots, ...)
"""

PATH_PLOTS = "ml/plots/"
makedirs(PATH_PLOTS, exist_ok=True)

TEST_DATA = [                      
                        [1, 101, 102, "reliability", 100, 0],
                        [2, 108, 107, "reliability", 10, 0],
                        [0, 100, 102, "reliability", 70, 0],
                        # [0, 104, 105, "reliability", 70, 0],
                        # [0, 109, 110, "reliability", 50, 0],
                        # [2, 107, 108, "reliability", 10, 0],
                        # [1, 100, 101, "reliability", 100, 0],
                        # [3, 200, 201, "reliability", 85, 0],
                        ]

                # [1, 100, 101, "reliability", 100, 0],
                # [2, 100, 101, "reliability", 100, 0],
                # [3, 100, 101, "reliability", 100, 0],
                # [4, 100, 101, "reliability", 100, 0],
                # [1, 100, 101, "importance", 100, 0],
                # [1, 100, 101, "reliability", 100, 0],
                # [1, 102, 103, "reliability", 70, 0],
                # [2, 104, 105, "reliability", 50, 0],
                # [3, 106, 107, "reliability", 30, 0],
                # [4, 108, 109, "reliability", 30, 0],
                # [67, 200, 201, "reliability", 0, 0]
             #+ [[0, 555, 556, "reliability", 40, 0]] * 10 

NAME = ""
EPOCHS = 50

def run_experiment(comparison_data):
    """ trains and outputs some stats """
    if True:
        from ..core import ml_run
        seedall(789144)
        # fake_data, glob_fake, loc_fake = generate_data(1000, 30, 10, dens=0.8,
        #                                                 noise=0.001)
        glob_scores, contributor_scores = ml_run(   comparison_data,
                                                    EPOCHS,
                                                    ["reliability"], 
                                                    resume=False,
                                                    save=True,
                                                    verb=1)
        save_to_json(glob_scores, contributor_scores, NAME)
    else:
        glob_scores, contributor_scores = load_from_json(NAME)
    # for c in comparison_data:
    #     if c[3]=="largely_recommended":
    #         print(c)
    # disp_fake_pred(glob_fake, glob_scores)

    # disp_one_by_line(glob_scores)
    # disp_one_by_line(contributor_scores)
    print("glob:", len(glob_scores), "local:",  len(contributor_scores))

def licch_stats(licch):
    ''' gives some statistics about Licchavi object '''
    print('LICCH_SATS')
    licch.check() # some tests
    h = licch.history
    print("nb_nodes", licch.nb_nodes)
    licch.stat_s()  # print stats on s parameters
    with torch.no_grad():
        gen_s = licch.all_nodes("s")
        l_s = [s.item() for s in gen_s]
        plot_density(   l_s, 
                        "s parameters", 
                        PATH_PLOTS,
                        "s_params.png")
    plot_metrics([h], path=PATH_PLOTS)
    # print("uncertainty", licch.uncert)

def scores_stats(glob_scores):
    ''' gives statistics on global scores
    
    glob_scores: torch tensor of global scores
    '''
    print('SCORES_STATS')
    var = torch.var(glob_scores)
    mini, maxi = (torch.min(glob_scores).item(),  
                torch.max(glob_scores).item() )
    print("minimax:", mini, maxi)
    print("variance of global scores :", var.item())
    with torch.no_grad():
        plot_density(glob_scores.cpu(), "Global scores", PATH_PLOTS, "scores.png")
