from torch.autograd.functional import hessian
import torch
from copy import deepcopy

from .losses import round_loss, loss_fit_s_gen, loss_gen_reg

"""
Metrics used for training monitoring in "licchavi.py"

Main file "ml_train.py"
"""

# metrics on models
def extract_grad(model):
    ''' returns list of gradients of a model 
    
    model (float tensor): torch tensor with gradients

    Returns:
        (float tensor list): list of gradients of the model
    '''
    return [p.grad for p in [model]]

def scalar_product(l_grad1, l_grad2):
    ''' scalar product of 2 lists of gradients 
    
    l_grad1 (float tensor list): list of gradients of a model
    l_grad2 (float tensor list): list of gradients of a model

    Returns:
        (float): scalar product of the gradients
    '''
    s = 0
    for g1, g2 in zip(l_grad1, l_grad2):
        s += (g1 * g2).sum()
    return round_loss(s, 4)

def replace_coordinate(tens, score, idx):
    """ Replaces one coordinate of the tensor

    Args:
        tens (float tensor): local model
        score (scalar tensor): score to put in tens
        idx (int): index of score to replace

    Returns:
        (float tensor): same tensor as input but backward pointing to -score
    """
    size = len(tens)
    left, _, right = torch.split(tens, [idx, 1, size - idx - 1]) 
    return torch.cat([left, score, right])

# ------ to compute uncertainty -------
def get_hessian_fun(nodes, general_model, fit_scale, gen_scale, pow_gen,
                          id_node, vidx):
    """ Gives loss in function of local model for hessian computation 
    
    Args:
        nodes (Node dictionnary): dictionnary of all nodes
        general_model (float tensor): general model
        fit_scale (float): importance of the local loss
        gen_scale float): importance of the generalisation loss
        pow_gen (float, float): distance used for generalisation
        id_node (int): id of user
        vidx (int): index of video, ie index of parameter

    Returns:
        (scalar tensor -> float) function giving loss according to one parameter 
    """
    def get_loss(score):
        """ Used to compute its second derivative to get uncertainty
        
        input (float scalar tensor): one score

        Returns:
            (float scalar tensor): partial loss
        """

        new_model = replace_coordinate(nodes[id_node].model, score, vidx)
        nodes[id_node].model = new_model
        fit_loss, s_loss, gen_loss =  loss_fit_s_gen(nodes, general_model, fit_scale, 
                                    gen_scale, pow_gen)
        return fit_loss + s_loss + gen_loss
    return get_loss

def get_uncertainty(nodes, general_model, fit_scale, 
                        gen_scale, pow_gen, vid_vidx):
    """ Returns uncertainty for all local scores 
    
    Args:
        nodes (Node dictionnary): dictionnary of all nodes
        general_model (float tensor): general model
        fit_scale (float): importance of the local loss
        gen_scale float): importance of the generalisation loss
        pow_gen (float, float): distance used for generalisation
        vid_vidx (dictionnary): {video ID: video index}

    Returns:
        (list of list of float): uncertainty for all local scores
    """
    l_uncert = []
    for uid, node in nodes.items(): # for all nodes
        local_uncerts = []
        for vid in node.vids:  # for all videos of the node
            vidx = vid_vidx[vid]  # video index
            score = node.model[vidx:vidx+1].detach()
            score = deepcopy(score)
            fun = get_hessian_fun(nodes, general_model, fit_scale,
                                gen_scale, pow_gen, uid, vidx)
            deriv2 = hessian(fun, score).item()
            uncert = deriv2**(-0.5)
            local_uncerts.append(uncert)
        l_uncert.append(local_uncerts)
    return l_uncert

# -------- to check equilibrium ------
def _random_signs(epsilon, nb_vids):
    """ Returns a tensor whith binary random coordinates

    epsilon (float): scores increment before computing gradient
    nb_vids (int): length of output tensor

    Returns:
        (float tensor): coordinates are +/-epsilon randomly
    """
    rand = torch.randint(2, size=(1,nb_vids))[0] - 0.5
    return rand * 2 * epsilon

# FIXME factorize check_equilibrium glob and loc

def check_equilibrium_glob(epsilon, nodes, general_model, opt_gen,
                            gen_scale, reg_scale, 
                            pow_gen, pow_reg, vid_vidx):
    """ Returns proportion of global scores which have converged  
    
    Args:
        epsilon (float): scores increment before computing gradient
        nodes (Node dictionnary): dictionnary of all nodes
        general_model (float tensor): general model
        fit_scale (float): importance of the local loss
        gen_scale float): importance of the generalisation loss
        reg_scale float): importance of the regulation
        pow_gen (float, float): distance used for generalisation
        vid_vidx (dictionnary): {video ID: video index}

    Returns:
        (float): fraction of scores at equilibrium
    """
    nbvid = len(vid_vidx)
    incr = _random_signs(epsilon, nbvid)

    def _one_side_glob(increment, general_model):
        """ increment (float tensor): coordinates are +/- epsilon """
        for node in nodes.values():
            node.opt.zero_grad(set_to_none=True)  # node optimizer 
        opt_gen.zero_grad(set_to_none=True)  # general optimizer
        # adding epsilon to scores
        with torch.no_grad():
            general_model += increment

        gen_loss, reg_loss = loss_gen_reg(
            nodes, general_model,
            gen_scale, reg_scale,
            pow_gen, pow_reg
        )

        loss = gen_loss + reg_loss
        loss.backward()

        glob_derivs = general_model.grad * increment
        # removing epsilon from score
        with torch.no_grad():
            general_model -= increment
        return glob_derivs

    derivs1 = _one_side_glob(incr, general_model)
    derivs2 = _one_side_glob(-incr, general_model)
    both = torch.logical_and(derivs1 > 0, derivs2 > 0)
    frac_glob = torch.count_nonzero(both) / nbvid

    return frac_glob.item()

def check_equilibrium_loc(epsilon, nodes, general_model, opt_gen,
                            fit_scale, gen_scale, 
                            pow_gen, vid_vidx):
    """ Returns proportion of local scores which have converged 
    
    Args:
        epsilon (float): scores increment before computing gradient
        nodes (Node dictionnary): dictionnary of all nodes
        general_model (float tensor): general model
        fit_scale (float): importance of the local loss
        gen_scale float): importance of the generalisation loss
        reg_scale float): importance of the regulation
        pow_gen (float, float): distance used for generalisation
        vid_vidx (dictionnary): {video ID: video index}

    Returns:
        (float): fraction of scores at equilibrium
    """
    nbvid = len(vid_vidx)
    nbn = len(nodes)
    incr = _random_signs(epsilon, nbvid)

    def _one_side_loc(increment):
        """ increment (float tensor): coordinates are +/- epsilon """
        l_derivs = torch.empty(nbn, nbvid)
        # resetting gradients
        for node in nodes.values():
            node.opt.zero_grad(set_to_none=True)  # node optimizer 
        opt_gen.zero_grad(set_to_none=True) # general optimizer
        # adding epsilon to scores
        with torch.no_grad():
            for node in nodes.values():
                node.model += increment
        # computing gradients
        fit_loss, _, gen_loss = loss_fit_s_gen(nodes, general_model, 
                                    fit_scale, gen_scale, pow_gen)
        loss = fit_loss + gen_loss
        loss.backward()
        # adding derivatives
        for uidx, node in enumerate(nodes.values()):
            l_derivs[uidx] = node.model.grad * increment
        # removing epsilon from score
        with torch.no_grad():
            for node in nodes.values():
                node.model -= increment
        return l_derivs

    derivs1 = _one_side_loc(incr)
    derivs2 = _one_side_loc(-incr)
    both = torch.logical_and(derivs1 > 0, derivs2 > 0)
    used = torch.logical_or(derivs1 != 0, derivs2 != 0)
    frac_loc = torch.count_nonzero(both) / torch.count_nonzero(used)
    return frac_loc.item()
