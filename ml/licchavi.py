import torch
from copy import deepcopy
from time import time
import logging
from logging import info as loginf

from .losses import model_norm, round_loss, models_dist
from .losses import predict, loss_fit_s_gen, loss_gen_reg
from .metrics import extract_grad, scalar_product
from .metrics import check_equilibrium_glob, check_equilibrium_loc
from .data_utility import expand_tens, one_hot_vids
from .hyperparameters import get_defaults, get_model, get_s
from .nodes import Node
from .dev.visualisation import disp_one_by_line

"""
Machine Learning algorithm, used in "core.py"

Organisation:
- ML model and decentralised structure are here
- Main file is "ml_train.py"

Structure:
- Licchavi class is the structure designed to include
    a global model and one for each node
-- read Licchavi __init__ comments to better understand

USAGE:
- hardcode training hyperparameters in "hyperparameters.py"
- use get_licchavi() to get an empty Licchavi structure
- use Licchavi.set_allnodes() to populate nodes
- use Licchavi.train() to train the models
- use Licchavi.output_scores() to get the results
"""

# nodes organisation
class Licchavi():
    ''' Training structure including local models and general one '''
    def __init__(self, nb_vids, vid_vidx, crit, gpu=False, verb=1):
        ''' 
        nb_vids: number of different videos rated by at least one contributor 
                    for this criteria
        vid_vidx: dictionnary of {vID: idx}
        crit: comparison criteria learnt
        '''
        self.verb = 1
        self.nb_params = nb_vids  # number of parameters of the model
        self.vid_vidx = vid_vidx # {video ID : video index (for that criteria)}
        self.criteria = crit # criteria learnt by this Licchavi
        self.gpu = gpu # boolean for gpu usage (not implemented yet)

        self.opt = torch.optim.SGD   # optimizer
        # defined in "hyperparameters.py"

        self.lr_node = 0    # local learning rate (local scores)
        self.lr_s = 0     # local learning rate for s parameter
        self.lr_gen = 0  # global learning rate (global scores)
        self.gen_freq = 0  # generalisation frequency (>=1)
        self.w0 = 0     # regularisation strength
        self.w = 0    # default weight for a node

        self.get_model = get_model # neural network to use
        self.general_model = self.get_model(nb_vids, gpu)
        self.init_model = deepcopy(self.general_model) # saved for metrics
        self.last_grad = None
        self.opt_gen = self.opt([self.general_model], lr=self.lr_gen)
        self.pow_gen = (1,1)  # choice of norms for Licchavi loss 
        self.pow_reg = (2,1)  # (internal power, external power)

        self.nb_nodes = 0
        self.nodes = {}
        self.history = ([], [], [], [], [], [], [], [])  # metrics
        self.users = []  # user IDs

        # defaults hyperparameters from "hyperparameters.py"
        self.set_params(**get_defaults())

    def set_params(self, **params):
        """ set training hyperparameters """
        for k, v in params.items():
            setattr(self, k, v)

    # ------------ input and output --------------------
    def _get_default(self):
        ''' Returns: - (default s, default model, default age) '''
        model_plus = (
            get_s(self.gpu), # s
            self.get_model(self.nb_params, self.gpu), # model
            0 #age
        )
        return model_plus

    def _get_saved(self, loc_models_old, id, nb_new):
        ''' Returns saved parameters updated or default
        
        loc_models_old (dictionnary): saved parameters in dictionnary of tuples
                                        {user ID: (s, model, age)}
        id (int): id of node (user)
        nb_new (int): number of new videos (since save)

        Returns:
            (s, model, age), updated or default
        '''
        if id in loc_models_old:
            s, mod, age = loc_models_old[id]
            mod = expand_tens(mod, nb_new, self.gpu)
            triple = (s, mod, age)
        else:
            triple = self._get_default()
        return triple

    def set_allnodes(self, data_dic, user_ids):
        ''' Puts data in Licchavi and create a model for each node 
        
        data_dic (dictionnary): {userID: (vID1_batch, vID2_batch, 
                                            rating_batch, single_vIDs, masks)}              
        users_ids (int array): users IDs  
        '''
        nb = len(data_dic)
        self.nb_nodes = nb
        self.users = user_ids
        self.nodes = {id: Node( *data, 
                                *self._get_default(), 
                                self.w,
                                self.lr_node,
                                self.lr_s,
                                self.opt
                        ) for id, data in zip(user_ids, data_dic.values())}         
        if self.verb >= 1:
            loginf("Total number of nodes : {}".format(self.nb_nodes))

    def load_and_update(self, data_dic, user_ids, fullpath):
        ''' Loads weights and expands them as required 

        data_dic (dictionnary):  {userID: (vID1_batch, vID2_batch, 
                                    rating_batch, single_vIDs, masks)}
        user_ids (int array): users IDs
        '''
        loginf('Loading models')
        saved_data = torch.load(fullpath)
        self.criteria, dic_old, gen_model_old, loc_models_old = saved_data
        nb_new = self.nb_params - len(dic_old) # number of new videos
        # initialize scores for new videos
        self.general_model = expand_tens(gen_model_old, nb_new) 
        self.opt_gen = self.opt([self.general_model], lr=self.lr_gen)
        self.users = user_ids
        nbn = len(user_ids)
        self.nb_nodes = nbn
        self.nodes = {
            id: Node(*data,
                     *self._get_saved(loc_models_old, id, nb_new),
                     self.w,
                     self.lr_node,
                     self.lr_s,
                     self.opt
               ) for id, data in zip(user_ids, data_dic.values())
        }
        if self.verb >= 1:
            loginf(f"Total number of nodes : {self.nb_nodes}")
        loginf('Models updated')

    def output_scores(self):
        ''' Returns video scores both global and local
        
        Returns :   
        - (tensor of all vIDS , tensor of global video scores)
        - (list of tensor of local vIDs , list of tensors of local video scores)
        '''
        local_scores = []
        list_ids_batchs = []

        with torch.no_grad():
            glob_scores = self.general_model
            for node in self.nodes.values():
                input = one_hot_vids(self.vid_vidx, node.vids, self.gpu)
                output = predict(input, node.model) 
                local_scores.append(output)
                list_ids_batchs.append(node.vids)
            vids_batch = list(self.vid_vidx.keys())

        return (vids_batch, glob_scores), (list_ids_batchs, local_scores)

    def save_models(self, fullpath):
        ''' Saves age and global and local weights, detached (no gradients) '''
        loginf('Saving models')
        local_data = {id:  (node.s,            # s
                            node.model.detach(),   # model
                            node.age            # age
                        ) for id, node in self.nodes.items()}
        saved_data = (  self.criteria,
                        self.vid_vidx,
                        self.general_model.detach(), 
                        local_data
                        )
        torch.save(saved_data, fullpath)
        loginf('Models saved')

    # --------- utility --------------
    def all_nodes(self, key):
        ''' Returns a generator of one parameter for all nodes '''
        for node in self.nodes.values():
            yield getattr(node, key)
    
    def stat_s(self):
        ''' Prints s stats '''
        l_s = [(round_loss(s, 2), id) for s, id in zip(self.all_nodes("s"), 
                                                        self.nodes.keys() )]
        tens = torch.tensor(l_s)
        disp_one_by_line(l_s)
        tens = tens[:, 0]
        print("mean of s: ", round_loss(torch.mean(tens), 2))
        print("min and max of s: ", round_loss(torch.min(tens), 2), 
                                    round_loss(torch.max(tens), 2))
        print("var of s: ", round_loss(torch.var(tens), 2))

    # ---------- methods for training ------------
    def _set_lr(self):
        ''' Sets learning rates of optimizers according to Licchavi settings '''
        for node in self.nodes.values(): 
            node.opt.param_groups[0]['lr'] = self.lr_node # node optimizer
            # FIXME update lr_s (not useful currently)
        self.opt_gen.param_groups[0]['lr'] = self.lr_gen

    def _lr_schedule(self, epoch, fit_scale, gen_scale, reg_scale):
        """ Changes learning rates in a (hopefully) smart way 
        
        epoch (int): current epoch
        fit_scale (float): fitting loss multiplier
        gen_scale (float): generalisation loss multiplier
        reg_scale (float): regularisation loss multiplier
        self.verb (int): self.verbosity level

        Returns:
            (bool): True for an early stopping
        """
        decay_rush = 0.97
        decay_fine = 0.8
        # min_lr_rush = 0.05 FIXME add in the future
        min_lr_fine = 0.001
        precision = 0.96 # proportion of parameters at eq for early stopping
        lr_rush_duration = 8

        # phase 1  : rush (high lr to increase l2 norm fast)
        if epoch <= lr_rush_duration:
            self.lr_gen *= decay_rush
            self.lr_node *= decay_rush

        # phase 2 : fine tuning (low lr), we monitor equilibrium for early stop
        elif epoch % 2 == 0:
            if self.lr_node >= min_lr_fine / decay_fine:
                self.lr_gen *= decay_fine
                self.lr_node *= decay_fine

            epsilon = 0.1
            frac_glob = check_equilibrium_glob(
                epsilon,
                self.nodes, self.general_model, self.opt_gen,
                gen_scale, reg_scale,
                self.pow_gen, self.pow_reg, self.vid_vidx
            )

            if self.verb >= 1:
                loginf(f'Equilibrium glob({epsilon}): {round(frac_glob,3)}')
            if frac_glob > precision:
                frac_loc = check_equilibrium_loc(
                    epsilon,
                    self.nodes, self.general_model, self.opt_gen,
                    fit_scale, gen_scale,
                    self.pow_gen, self.vid_vidx
                )
                if self.verb >= 1:
                    loginf(f'Equilibrium loc({epsilon}): {round(frac_loc,3)}')
                if frac_loc > precision:
                    loginf('Early Stopping')
                    return True
        return False

    def _zero_opt(self):
        ''' Sets gradients of all models '''
        for node in self.nodes.values():
            node.opt.zero_grad(set_to_none=True)  # node optimizer 
        self.opt_gen.zero_grad(set_to_none=True) # general optimizer

    def _update_hist(self, epoch, fit, s, gen, reg):
        ''' Updates history (at end of epoch) '''
        self.history[0].append(round_loss(fit))
        self.history[1].append(round_loss(s))
        self.history[2].append(round_loss(gen))
        self.history[3].append(round_loss(reg))
        dist = models_dist(self.init_model, self.general_model, pow=(2,0.5)) 
        norm = model_norm(self.general_model, pow=(2,0.5))
        self.history[4].append(round_loss(dist, 3))
        self.history[5].append(round_loss(norm, 3))
        grad_gen = extract_grad(self.general_model)
        if epoch > 1: # no previous model for first epoch
            scal_grad = scalar_product(self.last_grad, grad_gen)
            self.history[6].append(scal_grad)
        else:
            self.history[6].append(0) # default value for first epoch
        self.last_grad = deepcopy(extract_grad(self.general_model)) 
        grad_norm = scalar_product(grad_gen, grad_gen)  #FIXME use sqrt ?
        self.history[7].append(grad_norm)

    def _old(self, years):
        ''' Increments age of nodes (during training) '''
        for node in self.nodes.values():
            node.age += years 

    def _do_step(self, fit_step):
        ''' Makes step for appropriate optimizer(s) '''
        if fit_step:  # updating local or global alternatively
            for node in self.nodes.values(): 
                node.opt.step() # node optimizer   
        else:
            self.opt_gen.step()

    def _regul_s(self):
        """ regulate s parameters """
        for node in self.nodes.values():
            if node.s <= 0:
                with torch.no_grad():
                    node.s[0] = 0.4
                    logging.warning('Regulating negative s')

    def _print_losses(self, tot, fit, s, gen, reg):
        ''' Prints losses into log info '''
        fit, s = round_loss(fit, 2), round_loss(s, 2)
        gen, reg = round_loss(gen, 2), round_loss(reg, 2)

        loginf(f'total loss : {tot}\nfitting : {fit}, '
               f's : {s}, generalisation : {gen}, regularisation : {reg}')

    # ====================  TRAINING ================== 

    def train(self, nb_epochs=2):
        ''' training loop '''
        loginf('STARTING TRAINING')

        time_train = time()

        # initialisation to avoid undefined variables at epoch 1
        loss, fit_loss, s_loss, gen_loss, reg_loss = 0, 0, 0, 0, 0
        # FIXME make scales attributes or remove altogether (too heavy)
        fit_scale = 1 
        gen_scale = 1  # node weights are used in addition
        reg_scale = self.w0

        reg_loss = reg_scale * model_norm(self.general_model, self.pow_reg)  

        # training loop 
        nb_steps = self.gen_freq + 1 # one fitting step 
        for epoch in range(1, nb_epochs + 1):
            early_stop = self._lr_schedule( epoch, fit_scale, 
                                            gen_scale, reg_scale, self.verb)
            if early_stop:
                break # don't do this epoch nor any other
            self._set_lr()
            self._regul_s()

            if self.verb >= 1:
                loginf("epoch {}/{}".format(epoch, nb_epochs))
            time_ep = time()

            for step in range(1, nb_steps + 1):
                fit_step = (step == 1) # fitting on first step only
                if self.verb >= 2:
                    txt = "(fit)" if fit_step else "(gen)" 
                    loginf(f'step : {step}/{nb_steps} {txt}')
                self._zero_opt() # resetting gradients

                # ----------------    Licchavi loss  -------------------------
                # only first 3 terms of loss updated
                if fit_step:
                    fit_loss, s_loss, gen_loss = loss_fit_s_gen(
                        self.nodes,
                        self.general_model,
                        fit_scale,
                        gen_scale,
                        self.pow_gen
                    )
                    loss = fit_loss + s_loss + gen_loss                    
                # only last 2 terms of loss updated 
                else:        
                    gen_loss, reg_loss = loss_gen_reg(
                        self.nodes,
                        self.general_model,
                        gen_scale,
                        reg_scale,
                        self.pow_gen,
                        self.pow_reg
                    )
                    loss = gen_loss + reg_loss

                if self.verb >= 2:
                    total_loss = round_loss(fit_loss + s_loss
                                            + gen_loss + reg_loss)
                    self._print_losses(total_loss, fit_loss, s_loss, 
                                        gen_loss, reg_loss)           
                # Gradient descent 
                loss.backward() 
                self._do_step(fit_step)   

            self._update_hist(epoch, fit_loss, s_loss, gen_loss, reg_loss)
            self._old(1)  # aging all nodes of 1 epoch
            if self.verb >= 1.5:
                loginf(f'epoch time :{round(time() - time_ep, 2)}') 

        # ----------------- end of training ------------------------------- 
        loginf('END OF TRAINING') 
        loginf(f'training time :{round(time() - time_train, 2)}')
        return self.history # self.train() returns lists of metrics

    # ------------ to check for problems --------------------------
    def check(self):
        ''' Performs some tests on internal parameters adequation '''
        # population check
        b1 = (self.nb_nodes == len(self.nodes))
        # history check
        reference = self.history[0]
        b2 = all([len(l) == len(reference) for l in self.history])

        if (b1 and b2):
            loginf("No Problem")
        else:
            logging.warning("Coherency problem in Licchavi object ")
