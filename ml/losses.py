import torch
from time import time

"""
Losses used in "licchavi.py"

Main file is "ml_train.py"
"""
def predict(input, tens, mask=None):
    ''' Predicts score according to a model

    Args:
        input (bool 2D tensor): one line is a one-hot encoded video index
        tens (float tensor): tensor = model
        mask (bool tensor): one element is bool for using this comparison

    Returns: 
        (2D float tensor): score of the videos according to the model
    '''
    if input.shape[1] == 0:  # if empty input
        return torch.zeros((1, 1))
    if mask is not None:
        return torch.where(
            mask,
            torch.matmul(input.float(), tens),
            torch.zeros(1)
        )
    return torch.matmul(input.float(), tens)

# losses (used in licchavi.py)
def fbbt(t,r):
    ''' fbbt loss function (used for test only)

    Used only for testing

    Args:
        t (float tensor): batch of (s * (ya - yb))
        r (float tensor): batch of ratings given by user.

    Returns:
        (float tensor): sum of empirical losses for all comparisons of one user
    '''
    two = torch.tensor(2)
    losses = torch.log(abs(torch.sinh(t)/t)) + r * t + torch.log(two)
    return sum(losses)

#FIXME clean this function
@torch.jit.script # to optimize computation time
def hfbbt(t,r):
    ''' approximated fbbt loss function (used in Licchavi)
    
    Args:
        t (float tensor): batch of (s * (ya - yb))
        r (float tensor): batch of ratings given by user.

    Returns:
        (float tensor): sum of empirical losses for all comparisons of one user
    '''

    small = abs(t) <= 0.01
    medium = torch.logical_and((abs(t) < 10) ,(abs(t) > 0.01))
    big = abs(t) >= 10
    zer = torch.zeros(1)
    loss = 0

    a = torch.where(    small,
                        t**2 / 6 + r *t + torch.log(torch.tensor(2)),
                        zer)
    loss += a.sum()
    tt = torch.where(t!=0, t, torch.ones(1)) # trick to avoid zeros so Nan
    b = torch.where(medium, 
                    torch.log(2 * torch.sinh(tt) / tt) + r * tt,
                    zer)
    loss += b.sum()
    c = torch.where(big, abs(tt) - torch.log(abs(tt)) + r * tt, zer)
    loss += c.sum()
    return loss

def get_fit_loss(model, s, a_batch, b_batch, r_batch, vidx=-1):
    """ Fitting loss for one node

    Args:
        model (float tensor): node local model.
        s (float tensor): s parameter.
        a_batch (bool 2D tensor): first videos compared by user.
        b_batch (bool 2D tensor): second videos compared by user.
        r_batch (float tensor): rating provided by user.

    Returns:
        (float scalar tensor): fitting loss.
    """
    if vidx != -1:
        # never called

        idxs = torch.tensor(
            [idx for idx, ab in enumerate(zip(a_batch, b_batch)) if (ab[0][vidx] or ab[1][vidx])],
            dtype=torch.long
        )

        if idxs.shape[0] == 0: # if user didnt rate video
            return torch.scalar_tensor(0)

        ya_batch = predict(a_batch[idxs], model)
        yb_batch = predict(b_batch[idxs], model)
        loss = hfbbt(s * (ya_batch - yb_batch), r_batch[idxs])
    else:
        ya_batch = predict(a_batch, model)
        yb_batch = predict(b_batch, model)
        loss = hfbbt(s * (ya_batch - yb_batch), r_batch)
    return loss

def get_s_loss(s):
    ''' Scaling loss for one node
    
    Args:
        s (float tensor): s parameter.
    
    Returns:
        float tensor: second half of local loss
    '''
    return 0.5 * s**2 - torch.log(s)

def models_dist(model1, model2, pow=(1,1), mask=None, vidx=-1):  
    ''' distance between 2 models (l1 by default)

    Args:
        model1 (float tensor): scoring model
        model2 (float tensor): scoring model
        pow (float, float): (internal power, external power)
        mask (bool tensor): subspace in which to compute distance
        vidx (int): video index if only one is computed (-1 for all)
    
    Returns:
        (scalar float tensor): distance between the 2 models
    '''
    q, p = pow
    if vidx == -1: # if we want several coordinates
        if mask is None: # if we want all coordinates
            dist = ((model1 - model2)**q).abs().sum()**p
        else:
            dist = (((model1 - model2) * mask)**q).abs().sum()**p            
    else: # if we want only one coordinate
        # never called
        dist = abs(model1[vidx] - model2[vidx])**(q*p)
    return dist

def model_norm(model, pow=(2,1), vidx=-1): 
    ''' norm of a model (l2 squared by default)

    Args:
        model (float tensor): scoring model
        pow (float, float): (internal power, external power)
        vidx (int): video index if only one is computed (-1 for all)

    Returns: 
        (float scalar tensor): norm of the model
    '''
    q, p = pow
    if vidx != -1: # if we use only one video
        return abs(model[vidx])**(q*p)
    return (model**q).abs().sum()**p

# losses used in "licchavi.py"
def loss_fit_s_gen(nodes, general_model, fit_scale, gen_scale, pow_gen,
                   vidx=-1):
    """ Computes local and generalisation terms of loss
    
    Args:
        nodes (dictionnary): {node ID: Node()}
        general_model (float tensor): general score model
        fit_scale (float): fitting loss multiplier
        gen_scale (float): generalisation loss multiplier
        pow_gen (float, float): parameters of generalisation distance
        vidx (int): video index if we are interested in partial loss 
                                    (-1 for all indexes)

    Returns:
        (float tensor): sum of local terms of loss
        (float tensor): generalisation term of loss
    """
    fit_loss, s_loss, gen_loss = 0, 0, 0
    for node in nodes.values():  
        fit_loss += get_fit_loss(   node.model, # local model
                                    node.s,     # s
                                    node.vid1,  # id_batch1
                                    node.vid2,  # id_batch2
                                    node.r,     # r_batch
                                    vidx)
        if vidx == -1: # only if all loss is computed
            s_loss += get_s_loss(node.s)         
        g = models_dist(node.model,    # local model
                        general_model, # general model
                        pow_gen,       # norm
                        node.mask,     # mask
                        vidx=vidx      # video index if we want partial loss
                        ) 
        gen_loss += node.w * g  # node weight  * generalisation term

    fit_loss *= fit_scale
    gen_loss *= gen_scale
    return fit_loss, s_loss, gen_loss

def loss_gen_reg(nodes, general_model, gen_scale, reg_scale, pow_gen, pow_reg,
                                                                    vidx=-1):
    """ Computes generalisation and regularisation terms of loss
    
    Args:
        nodes (dictionnary): {node ID: Node()}
        general_model (float tensor): general score model
        gen_scale (float): generalisation loss multiplier
        reg_scale (float): regularisation loss multiplier
        pow_gen (float, float): parameters of generalisation distance
        pow_reg (float, float): parameters of regularisation norm
        vidx (int): video index if we are interested in partial loss 
                            (-1 for all indexes)

    Returns:
        (float tensor): generalisation term of loss
        (float tensor): regularisation loss (of general model)
    """
    gen_loss, reg_loss = 0, 0
    for node in nodes.values():
        g = models_dist(node.model,    # local model
                        general_model, # general model
                        pow_gen,       # norm
                        node.mask,     # mask
                        vidx=vidx
                        )
        gen_loss += node.w * g  # node weight * generalisation term
    reg_loss = model_norm(general_model, pow_reg, vidx=vidx) 
    gen_loss *= gen_scale
    reg_loss *= reg_scale 
    return gen_loss, reg_loss

def round_loss(tens, dec=0): 
    ''' from an input scalar tensor or int/float returns rounded int/float '''
    if type(tens) is int or type(tens) is float:
        return round(tens, dec)
    else:
        return round(tens.item(), dec)
