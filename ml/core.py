import os
import logging
from time import time

from ml.licchavi import Licchavi
from ml.handle_data import select_criteria, shape_data
from ml.handle_data import distribute_data, distribute_data_from_save
from ml.handle_data import format_out_loc, format_out_glob
from ml.dev.experiments import licch_stats, scores_stats

FOLDER_PATH = "ml/checkpoints/" 
FILENAME = "models_weights"
PATH = FOLDER_PATH + FILENAME
os.makedirs(FOLDER_PATH, exist_ok=True)
logging.basicConfig(filename='ml/ml_logs.log', level=logging.INFO)

def _set_licchavi(
        comparison_data, crit,
        fullpath=None, resume=False, verb=2, gpu=False
):
    ''' Shapes data and inputs it in Licchavi to initialize

    comparison_data (list of lists): output of fetch_data()
    crit (str): rating criteria
    fullpath (str): path from which to load previous training
    resume (bool): wether to resume previous training or not
    verb (int): verbosity level
    gpu (bool): using gpu or not
    
    Returns :   
        (Licchavi()): Licchavi object initialized with data
        (int array): array of users IDs in order
    '''
    # shape data
    one_crit = select_criteria(comparison_data, crit)
    full_data = shape_data(one_crit)

    # set licchavi using data
    if resume:
        nodes_dic, users_ids, vid_vidx = distribute_data_from_save(full_data,
                                                                   fullpath,
                                                                   gpu)
        licch = Licchavi(len(vid_vidx), vid_vidx, crit)
        licch.load_and_update(nodes_dic, users_ids, fullpath, verb)
    else:
        nodes_dic, users_ids, vid_vidx = distribute_data(full_data, gpu)
        licch = Licchavi(len(vid_vidx), vid_vidx, crit, gpu)
        licch.set_allnodes(nodes_dic, users_ids, verb)

    return licch, users_ids # FIXME we can do without users_ids ?

def _train_predict(licch, epochs, fullpath=None, save=False, verb=2):
    ''' Trains models and returns video scores for one criteria

    licch (Licchavi()): licchavi object innitialized with data
    epochs (int): maximum number of training epochs
    fullpath (str): path where to save trained models
    save (bool): wether to save the result of training or not
    verb (int): verbosity level
    
    Returns :   
    - (list of all vIDS , tensor of global video scores)
    - (list of arrays of local vIDs , list of tensors of local video scores)
    '''
    _ = licch.train(epochs, verb=verb) 
    glob, loc = licch.output_scores()
    if save:
        licch.save_models(fullpath)
    if verb >= 1: # some prints and plots
        licch_stats(licch)
        scores_stats(glob[1])
    return glob, loc

def ml_run(comparison_data, epochs, criterias, 
            resume=False, save=True, verb=2, gpu=False):
    """ Runs the ml algorithm for all criterias
    
    comparison_data: output of fetch_data()
    epochs (int): number of epochs of gradient descent for Licchavi
    criterias (str list): list of criterias to compute
    resume (bool): wether to resume from save or not
    verb (int): verbosity level 

    Returns:
    - video_scores: list of [video_id: int, criteria_name: str, 
                                score: float, uncertainty: float]
    - contributor_rating_scores: list of 
    [   contributor_id: int, video_id: int, criteria_name: str, 
        score: float, uncertainty: float]
    """ # FIXME: not better to regroup contributors in same list or smthg ?
    ml_run_time = time()
    glob_scores, loc_scores = [], []

    for criteria in criterias:
        logging.info("PROCESSING " + criteria)
        fullpath = PATH + '_' + criteria
        # preparing data
        licch, users_ids = _set_licchavi(comparison_data, criteria,
                                         fullpath, resume, verb, gpu)
        # training and predicting
        glob, loc = _train_predict(licch, epochs, fullpath, save, verb)

        # putting in required shape for output
        out_glob = format_out_glob(glob, criteria) 
        out_loc = format_out_loc(loc, users_ids, criteria) 
        glob_scores += out_glob
        loc_scores += out_loc

    logging.info(f'ml_run() total time : {round(time() - ml_run_time)}')
    return glob_scores, loc_scores
