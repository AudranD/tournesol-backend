import torch

"""
To set hyperparameters for Licchavi object in "licchavi.py"

Main file is "ml_train.py"
"""

def get_defaults():
    """ defaults training parameters """
    defaults = {
                # "theoretical" hyperparameters
                "w0": 1,  # float >= 0, regularisation parameter
                "w": 1,   # float >= 0, harmonisation parameter

                # learning hyperparameters
                "lr_gen": 0.09,     # float > 0, learning rate of global model
                "lr_node": 0.9,    # float > 0, learning rate of local models
                "lr_s" : 0.1,   # float > 0, learning rate of s parameters
                "gen_freq": 1, # int >= 1, number of global steps 
                }
    return defaults

def get_model(nb_vids, gpu=False):
    if gpu:
        return torch.zeros(nb_vids, requires_grad=True, device='cuda')
    return torch.zeros(nb_vids, requires_grad=True)

def get_s(gpu=False):
    if gpu:
        return torch.ones(1, requires_grad=True, device='cuda')
    return torch.ones(1, requires_grad=True)
